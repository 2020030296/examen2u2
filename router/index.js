const express = require('express');
const bodyparser = require('body-parser');
const router = express.Router();

router.get('/', (req,res)=>{

    const valores={
        Num: req.query.Num,
        Nom: req.query.Nom,
        DOM: req.query.DOM,
        tipo: req.query.tipo,
        KC: req.query.KC,
        sub: req.query.sub,
        imp: req.query.imp,
        desc: req.query.desc,
        tot: req.query.tot,
        TP: req.query.TP,
        costo: req.query.costo
    }

    res.render('examen.html', valores)
})
router.post('/resultado', (req,res)=>{

    const valores={
        Num: req.body.Num,
        Nom: req.body.Nom,
        DOM: req.body.DOM,
        tipo: req.body.tipo,
        KC: req.body.KC,
        sub: req.body.sub,
        imp: req.body.imp,
        desc: req.body.desc,
        tot: req.body.tot,
        TP: req.body.TP,
        costo : req.body.costo
    }
    res.render('examen2.html', valores)
})

module.exports=router;
